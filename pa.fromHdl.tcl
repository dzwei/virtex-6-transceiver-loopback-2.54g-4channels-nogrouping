
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name minBelleIIvt6 -dir "/media/dewei_b1409/WorkDisk/WorkSpace/BelleII/MyCode/Xilinx/minBelleIIvt6/planAhead_run_3" -part xc6vhx380tff1923-2
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "XCVR_TOP.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {ipcore_dir/chipscope_icon.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {ipcore_dir/v6_gtxwizard_v1_12_gtx.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
add_files [list {ipcore_dir/v6_gtxwizard_v1_12/implement/rx_phase_align_fifo.ngc}]
add_files [list {ipcore_dir/v6_gtxwizard_v1_12/implement/ila.ngc}]
add_files [list {ipcore_dir/v6_gtxwizard_v1_12/implement/icon.ngc}]
add_files [list {ipcore_dir/v6_gtxwizard_v1_12/implement/data_vio.ngc}]
set hdlfile [add_files [list {ipcore_dir/v6_gtxwizard_v1_12/example_design/frame_gen.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {ipcore_dir/v6_gtxwizard_v1_12/example_design/frame_check.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {ipcore_dir/v6_gtxwizard_v1_12.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {ipcore_dir/v6_gtxwizard_v1_12/example_design/v6_gtxwizard_v1_12_top.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {ipcore_dir/chipscope_ila.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {source_code/DataStruct_param_def_header.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {source_code/lane_up_condition_checker.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {source_code/traffical.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {source_code/frame_gen.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {source_code/frame_check.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {source_code/XCVR_8B10B_interconnect.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {source_code/XCVR_top.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set_property top XCVR_TOP $srcset
add_files [list {XCVR_TOP.ucf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/chipscope_ila.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/chipscope_icon.ncf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc6vhx380tff1923-2
