library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

library work;
use work.DataStruct_param_def_header.all;--invoke our defined type and parameter

library UNISIM;
use UNISIM.vcomponents.all; --bufg oddr OBUFDES

entity XCVR_TOP is
    port (
        RST_N_in          : in  std_logic := '1' ;
        Ref_Clock_in      : in  std_logic;
        Ref_Clock_in_N    : in  std_logic;
        RX_ser_bank       : in  ser_data_men_bank;
        TX_ser_bnak       : out ser_data_men_bank;
        RX_ser_N_bank     : in  ser_data_men_bank;
        TX_ser_N_bnak     : out ser_data_men_bank;

        tx_Para_data_bank             : in  para_data_men_bank;
        rx_Para_data_bank             : out para_data_men_bank;
        ext_tx_para_data_clk_bank     : out ser_data_men_bank;
        ext_rx_para_data_clk_bank     : out ser_data_men_bank;
        tx_traffic_ready_ext_bank     : out ser_data_men_bank;
        rx_traffic_ready_ext_bank     : out ser_data_men_bank;

        error_cnt_ch_bank             : out para_data_men_bank;

        CLK_out    	                  : out STD_LOGIC;
        CLK_out_N			          : out STD_LOGIC;
        
        clock_test_out   : out STD_LOGIC;

        CLK_SEL          : out STD_LOGIC;
        CLK_SEL_127M     : out STD_LOGIC;
        CLK_SEL_254M     : out STD_LOGIC
    );
end entity XCVR_TOP ;

architecture XCVR_TOP_connect of XCVR_TOP is
    --para data
    signal tx_Para_data_bank_buf  : para_data_men_bank ; --:= (others=> (others => (others => '0')));
    signal rx_Para_data_bank_buf  : para_data_men_bank ; --:= (others=> (others => (others => '0')));

    signal error_cnt_ch_bank_buf  : para_data_men_bank ; --:= (others=> (others => (others => '0')));
    --ext clock
    signal ext_tx_para_data_clk_bank_buf     : ser_data_men_bank ;
    signal ext_rx_para_data_clk_bank_buf     : ser_data_men_bank ;

    signal tx_traffic_ready_ext_bank_buf     : ser_data_men_bank ;
    signal rx_traffic_ready_ext_bank_buf     : ser_data_men_bank ;
    
    --clock  and clock buffer  
    signal Ref_Clock_buffer_out         : std_logic := '0';
    signal Ref_Clock_buffer_out_div2    : std_logic := '0';
    --output clk to rj45
    signal bufg_clk_out                 : std_logic ;
    signal oddr_clk_out                 : std_logic ;
    signal Diff_CLK_P                   : std_logic ;
    signal Diff_CLK_N                   : std_logic ;
    --clock test
    signal test_Clock                   : std_logic ;
    signal test_Clock_buf_i             : std_logic ;
begin
    --set clock port on ut3
    CLK_SEL         <= '1';--'0' when use ext127 clk
    CLK_SEL_127M    <= '1';--'0' when use ext127 clk
    CLK_SEL_254M    <= '1';
    --connect ext para data 
    tx_Para_data_bank_buf <= tx_Para_data_bank  ;
    rx_Para_data_bank     <= rx_Para_data_bank_buf;
    --connect ext para data clk
    ext_tx_para_data_clk_bank  <= ext_tx_para_data_clk_bank_buf ;
    ext_rx_para_data_clk_bank  <= ext_rx_para_data_clk_bank_buf ;
    tx_traffic_ready_ext_bank  <= tx_traffic_ready_ext_bank_buf ;
    rx_traffic_ready_ext_bank  <= rx_traffic_ready_ext_bank_buf ;

    error_cnt_ch_bank  <= error_cnt_ch_bank_buf ;
    --connect XCVR
    Connect_XVCR_Module_loop : for i in 0 to (num_of_xcvr_bank_used - 1) generate
    XCVR_Module_gen : entity work.XCVR_8B10B_interconnect
        port map (
            RST_N                       => RST_N_in,             

            Ref_Clock                   => Ref_Clock_buffer_out,

            TX_para_external_ch         => tx_Para_data_bank_buf(i),
            RX_para_external_ch         => rx_Para_data_bank_buf(i),
            TX_para_external_clk_ch     => ext_tx_para_data_clk_bank_buf(i),
            RX_para_external_clk_ch     => ext_rx_para_data_clk_bank_buf(i),
            tx_traffic_ready_ext_ch     => tx_traffic_ready_ext_bank_buf(i),
            rx_traffic_ready_ext_ch     => rx_traffic_ready_ext_bank_buf(i),
            error_cnt_ch                => error_cnt_ch_bank_buf(i),

            RX_ser                      => RX_ser_bank(i),
            TX_ser                      => TX_ser_bnak(i),
            RX_ser_N                    => RX_ser_N_bank(i),
            TX_ser_N                    => TX_ser_N_bnak(i)
        );
    end generate Connect_XVCR_Module_loop;


    --tranceiver clock and RJ45 out below :
    q1_clk0_refclk_ibufds : IBUFDS_GTXE1
    port map(
        O                               =>      Ref_Clock_buffer_out,
        CEB                             =>      '0',--not(RST_N),
        I                               =>      Ref_Clock_in,  -- Connect to package pin AU37
        IB                              =>      Ref_Clock_in_N -- Connect to package pin AU38
    );

    bufg_i : BUFG
    port map(
        I       => Ref_Clock_buffer_out  ,
        O       => bufg_clk_out
    );   

    oddr_i : ODDR
    generic map(
        DDR_CLK_EDGE  => "OPPOSITE_EDGE",   --"OPPOSITE_EDGE" or "SAME_EDGE"
        INIT          => '0',               --Sets initial state of the Q output to 1'b0 or 1'b1
        SRTYPE        => "SYNC"             --Specifies "SYNC" or "ASYNC" set/reset
    )
    port map(
        Q       =>   oddr_clk_out,      --1 bit DDR output
        C       =>   bufg_clk_out,      --1 bit clock input
        CE      =>   RST_N_in,          --1 bit enable input (positive active)
        D1      =>   '0',               --1 bit data input (positive edge)
        D2      =>   '1',               --1 bit data input (negative edge)
        R       =>   not(RST_N_in),     --1 bit reset input
        S       =>   '0'                --1 bit set input
    );     

    obufds_i : OBUFDS 
    generic map(
        IOSTANDARD => "DEFAULT" 
    )
    port map(
        I       => oddr_clk_out  ,
        O       => Diff_CLK_P,
        OB      => Diff_CLK_N
    );  
    CLK_out     <= Diff_CLK_P ;
    CLK_out_N   <= Diff_CLK_N ;

    RJ45_connector_LED : entity work.clock_divider
    generic map (
        divider_rate    => 127000000
    )
    port map(
        RST_N           => RST_N_in,
        clk_in          => bufg_clk_out,
        clk_out         => test_Clock
    );

    test_Clock_buf_i <= test_Clock ;
    bufg_for_CLK : OBUF
    port map(
        I       => test_Clock_buf_i  ,
        O       => clock_test_out
    );  

end architecture XCVR_TOP_connect;
